#!/bin/bash

# Run config
source ./wft_conf.sh


################################################################################################################
####### notify functions - you can choose these you want or even write some yourself -- see wft_conf.sh :)
function notifyViaBeeping () { # Requires: pc speaker (should work on almost every computer)
	echo URL to your task: http://www.google-melange.com/$CONTEST/task/view/google/$CONTEST_YEAR/$task_id
	echo Press Ctrl-C to stop beeping. 

	break_on_ctrl_c=0
	while [ "$ctrl_c_pressed" == "0" ]; do
		echo -en "\007" # beep
		sleep 1s
	done
	ctrl_c_pressed=0
	break_on_ctrl_c=1
}
function notifyViaLibnotify () { # Dedicated for GNOME Shell, will work partly on Unity and Xfce, also probably KDE and other DEs with libnotify support
	# thanks to: Mateusz "m4tx" Maćkowski --- http://m4tx.pl
	notify-send --expire-time=3600000 --urgency=critical --icon=$IMAGE_FILE '$CONTEST_YEAR' "Task $task_id: $task_state! \nhttp://www.google-melange.com/$CONTEST/task/view/google/$CONTEST_YEAR/$task_id$@"
}
function notifyViaToast () { # requires: win8 or newer
	local toast_state=Timeout
	while [ "$toast_state" == "Timeout" ]; do
		toast_state=$(extbin/toast -t "Task ID: $task_id" -p $IMAGE_FILE -m "New task state: $task_state!
Click here to see your task." -w)
	done
	if [ "$toast_state" == "Activated" ]; then
		explorer http://www.google-melange.com/$CONTEST/task/view/google/$CONTEST_YEAR/$task_id
	fi
}
function notifyViaWinMsgBox () { # Requires: win 2000 or newer
	local user_answer=$(extbin/msgbox -t "$CONTEST_YEAR" \
-m "Task ID: $task_id
New task state: $task_state

Do you want to open task page in your browser?" \
-s "MB_YESNO MB_ICONINFORMATION MB_DEFBUTTON1 MB_TOPMOST")

	if [ "$user_answer" == "IDYES" ]; then
		explorer http://www.google-melange.com/$CONTEST/task/view/google/$CONTEST_YEAR/$task_id
	fi
}