#!/bin/bash

# config should be correct! script doesn't check it

IMAGE_FILE=$(pwd)/images/gci2013-logo.png	# absolute path to image file (used by some notifications)
TIMESTAMP=60s								# how frequent script should check task (format for "sleep" command)
CONTEST=gci									# will probably works for GSoC too (part of task URL)
YEAR=2013									# official contest year
TASK_ID_FILE=task.id						# see README.md for further information (Using section)
NOTIFY_METHOD=notifyViaBeeping				# defines notify method - see information below
RECONNECTING_TRIES=-1						# count of reconnecting tries after losing connection; -1 means infinity

#####################################################
# NOTIFY_METHOD could be:
#  notifyViaBeeping		-- beeps until Ctrl-C is pressed [default, cause should work on every system with pc speaker]
#  notifyViaToast		-- displays toast notifications (in upper right corner) until it's clicked [since win8]
#  notifyViaLibnotify	-- displays notify via libnotify [Linux graphics environment with support for libnotify]
#  notifyViaWinMsgBox	-- displays message box with notifacations [since win 2000]
#
# You can create your own notification method (see code in notification_methods.sh).
#####################################################


#####################################################
# Private part of config
CONTEST_YEAR=${CONTEST}${YEAR}
