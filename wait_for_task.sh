#!/bin/bash


echo
echo \(C\) 2013 Artur \"Mrowqa\" Jamro  -----  http://mrowqa.pl/
echo Tool for auto-checking whether GCI task has reviewed.
echo Version 1.57.4
echo

# Run config
source ./wft_conf.sh
source ./notification_methods.sh


########################################################
function getState() { # params: $task_id
	local task_id=$1
	result=$(wget -qO- http://www.google-melange.com/$CONTEST/task/view/google/$CONTEST_YEAR/$task_id 2> /dev/null | grep Status: | sed -r s/^.*\"emph\"\>//g | sed -r s/\<.*$//g)
	echo $result
}

function checkState() { # params: $task_state
	local task_state=$1
	local task_states=(Open ClaimRequested Claimed NeedsWork NeedsReview Unapproved Unpublished Reopened ActionNeeded AwaitingRegistration Invalid Closed)
	for((i=0; i<${#task_states[*]}; i++)); do
		if [ "$task_state" == "${task_states[$i]}" ]; then
			return 0
		fi
	done
	return 1
}

function onStateChange() { # params: $new_state, $prev_state
	local new_state=$1
	local prev_state=$2
	
	echo
	echo --- $(date)
	echo Task changed state!
	echo New state: $new_state
	$NOTIFY_METHOD # you can choose these functions you want, you can even write some yourself :)
}

########################################################
break_on_ctrl_c=1
ctrl_c_pressed=0
control_c() { # run if user hits control-c
	ctrl_c_pressed=1
	if [ "$break_on_ctrl_c" == "1" ]; then
		echo Ctrl-C interruption! Exiting...
		exit
	fi
}
# trap keyboard interrupt (control-c)
trap control_c SIGINT
########################################################
################################################################################################################

if [ -z "$1" ]; then
	echo Usage: wait_for_task \<task_id\>
	echo Usage: wait_for_task -
	echo
	echo If you choose second version, \<task_id\> will be read from "$TASK_ID_FILE" file.
	echo If you want to configure this script, go ahead, just open wft_conf.sh script in
	echo your favourite text editor and simply edit it.
	echo
	echo You can update utility by running \"git pull\" if you cloned repository and
	echo there\'s some new changes.
	exit
fi

task_id=$1

if [ "$task_id" == "-" ]; then
	if [ ! -e "$TASK_ID_FILE" ]; then
		echo File $TASK_ID_FILE doesn\'t exist! Cannot get task id.
		exit 1
	fi
	task_id=$(cat $TASK_ID_FILE)
fi

echo Waiting for task state changes, task: $task_id
echo Checking every $TIMESTAMP
echo Current time: $(date)

prev_state=$(getState $task_id) # check whether returned good state! -- check whether it's good ID!
if ! checkState $prev_state; then
	echo Given task id is invalid or there\'s no access to Internet!
	echo \(or CONTEST and/or YEAR in wft_conf.sh might be invalid\)
	exit 2
fi
if [ "$prev_state" == "Closed" ]; then
	echo Task $task_id is closed!
	exit 0
fi

echo Current state: $prev_state
echo

while [ "true" ]; do
	task_state=$(getState $task_id)
	if [ -z "$task_state" ]; then
		echo
		echo --- $(date)
		echo Lost connection during checking task state!
		for((i=1; i<=RECONNECTING_TRIES || RECONNECTING_TRIES==-1; i++)); do
			echo Reconnecting $i time...
			task_state=$(getState $task_id)
			if [ "$task_state" ]; then
				break
			fi
			sleep $TIMESTAMP
		done
		if [ -z "$task_state" ]; then
			echo Connection not retrieved! Exiting...
			exit 3
		fi
		echo Reconnected!
	fi
	if [ "$task_state" != "$prev_state" ]; then
		onStateChange $task_state $prev_state # wait for user interaction, don't check firstly once more state
		if [ "$task_state" == "Closed" ]; then
			break
		fi
		echo
		echo Still waiting for state changes...
	fi
	prev_state=$task_state
	sleep $TIMESTAMP
done

echo
echo Task $task_id closed!
