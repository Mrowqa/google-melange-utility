Google Melange Notifier
=======================

Google Melange Notifier is a simple command line utility for notifying GCI and WSoC students about task state changes.

##Current notify methods

* via beeping - beeps until Ctrl-C is pressed [default, cause should work on every system with pc speaker]
* via toast - displays toast notifications (in upper right corner) until it's clicked [since win8]
* via libnotify - displays notify via libnotify [Linux graphics environment with support for libnotify]
* via Windows message box - displays message box with notifacations [since win 2000]

You can also add your own notification method by editing "notification_methods.sh" script. Then you can
change current notification method by editing NOTIFICATION_METHOD in "wft_conf.sh" file.

#Installing

##Windows

Since utility is written in bash, you need this shell (Windows doesn't have it by default). I recommend you git bash
because it's included in git and I use this, so you're sure that will work. If you cloned this from online repository
with git you probably have git bash.

You need wget too. You can download it [here](http://gnuwin32.sourceforge.net/packages.html). After unpacking it's
recommended to copy files to new directory and add package bin directory to PATH enviroment variable. You can also copy
content of the package bin directory to the same directory which "wait_for_task.sh" script is located in.
CAUTION! After running wget it will display errors about missing DLLs. You have to download them too ([the same URL](http://gnuwin32.sourceforge.net/packages.html)).

##Linux

Linux has bash and wget, so only thing you must do is clone this repository. If you don't have git, just install it
using your package manager.

##Other systems

You'll need bash (or similar shell) and wget command. To clone this repository you need git.

#Configuring

It's very simply! Just open "wft_conf.sh" file in your favourite text editor and simply edit it! There's instruction there.

#Using

Just run

	./wait_for_task.sh <task_id>

Task ID is part of task URL after last slash. If you won't paste ID every time you run script, you can save it
in TASK_ID_FILE (variable in "wft_conf.sh", "task.id" by default). Well, it's easier. Just run
once for save settings:

	echo <task_id> > task.id

Now you can run script with '-' (dash) parameter. It loads saved task ID.

	./wait_for_task.sh -

#Updating

If you cloned repository with git, you can download new changes by running

	git pull

#License

* GCI image belongs to Google.
* [Toast](https://github.com/nels-o/toaster) belongs to nels-o, license: [CC BY 3.0](http://creativecommons.org/licenses/by/3.0/us/)
* msgbox, wait_for_task.sh, wft_conf.sh belongs to Mrowqa (me), license: MIT (see LICENSE.txt file)



Copyright (C) 2013 Artur "Mrowqa" Jamro, <http://mrowqa.pl>
