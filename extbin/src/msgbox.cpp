#include <windows.h>
#include <iostream>
#include <string>
using namespace std;

UINT parseMbFlags(string flags);
UINT parseFlag(string flag);
string parseResultCode(int code);

int main(int argc, char* argv[])
{
	if(argc < 2)
	{
		cout << "Message Box command line utility.\n"
			"Copyright 2013 Artur \"Mrowqa\" Jamro, http://mrowqa.pl\n\n"
			"Usage: msgbox [-m <message>][-t <title>][-s <style>]\n"
			"<message> and <title> are normal strings.\n"
			"<style> is a string of uType flags separated by spaces (check MessageBox\n"
			"online documention on MSDN)\n\n"
			"Return value: please check MessageBox online documention on MSDN\n"
			"Utility prints adequate flag on stdout and returns adequate error code.\n"
			"If utility fail, the error code will equal 0 and printed message on stdout\n"
			"will be \"ERROR\". On stderr you will see error message.\n\n"
			"Example usage:\n"
			"msgbox -m \"Hello World!\" -t \"Hi!\" -s \"MB_OK MB_ICONINFORMATION\"\n";
		return 0;
	}

	try {
		string message, title, flags;
		for(int i=1; i<argc; i++)
		{
			string currentParameter = argv[i];

			if(currentParameter == "-t")
			{
				if(i+1 >= argc)
					throw string("Missing argument for -t parameter!");
				title = argv[++i];
			}
			else if(currentParameter == "-m")
			{
				if(i+1 >= argc)
					throw string("Missing argument for -m parameter!");
				message = argv[++i];
			}
			else if(currentParameter == "-s")
			{
				if(i+1 >= argc)
					throw string("Missing argument for -s parameter!");
				flags = argv[++i];
			}
			else
				throw string("Unrecognized parameter: " + currentParameter);
		}

		int result = MessageBoxA(nullptr, message.c_str(), title.c_str(), parseMbFlags(flags));
		cout << parseResultCode(result);
		return result;
	}
	catch(string msg)
	{
		cerr << msg << "\n";
		cout << "ERROR";
		return 0;
	}
}

UINT parseMbFlags(string flags)
{
	UINT result = 0;

	while(!flags.empty())
	{
		string currentFlag = flags.substr(0, flags.find(' '));
		flags.erase(0, currentFlag.size()+1);

		result |= parseFlag(currentFlag);
	}

	return result;
}

// Missing defines in MinGW
#define MB_CANCELTRYCONTINUE	0x00000006L

UINT parseFlag(string flag) // regex power!
{
		 if(flag == "MB_ABORTRETRYIGNORE")		return MB_ABORTRETRYIGNORE;
	else if(flag == "MB_CANCELTRYCONTINUE")		return MB_CANCELTRYCONTINUE;
	else if(flag == "MB_HELP")					return MB_HELP;
	else if(flag == "MB_OK")					return MB_OK;
	else if(flag == "MB_OKCANCEL")				return MB_OKCANCEL;
	else if(flag == "MB_RETRYCANCEL")			return MB_RETRYCANCEL;
	else if(flag == "MB_YESNO")					return MB_YESNO;
	else if(flag == "MB_YESNOCANCEL")			return MB_YESNOCANCEL;
	else if(flag == "MB_ICONEXCLAMATION")		return MB_ICONEXCLAMATION;
	else if(flag == "MB_ICONWARNING")			return MB_ICONWARNING;
	else if(flag == "MB_ICONINFORMATION")		return MB_ICONINFORMATION;
	else if(flag == "MB_ICONASTERISK")			return MB_ICONASTERISK;
	else if(flag == "MB_ICONQUESTION")			return MB_ICONQUESTION;
	else if(flag == "MB_ICONSTOP")				return MB_ICONSTOP;
	else if(flag == "MB_ICONERROR")				return MB_ICONERROR;
	else if(flag == "MB_ICONHAND")				return MB_ICONHAND;
	else if(flag == "MB_DEFBUTTON1")			return MB_DEFBUTTON1;
	else if(flag == "MB_DEFBUTTON2")			return MB_DEFBUTTON2;
	else if(flag == "MB_DEFBUTTON3")			return MB_DEFBUTTON3;
	else if(flag == "MB_DEFBUTTON4")			return MB_DEFBUTTON4;
	else if(flag == "MB_APPLMODAL")				return MB_APPLMODAL;
	else if(flag == "MB_SYSTEMMODAL")			return MB_SYSTEMMODAL;
	else if(flag == "MB_TASKMODAL")				return MB_TASKMODAL;
	else if(flag == "MB_DEFAULT_DESKTOP_ONLY")	return MB_DEFAULT_DESKTOP_ONLY;
	else if(flag == "MB_RIGHT")					return MB_RIGHT;
	else if(flag == "MB_RTLREADING")			return MB_RTLREADING;
	else if(flag == "MB_SETFOREGROUND")			return MB_SETFOREGROUND;
	else if(flag == "MB_TOPMOST")				return MB_TOPMOST;
	else if(flag == "MB_SERVICE_NOTIFICATION")	return MB_SERVICE_NOTIFICATION;

	return 0;
}


string parseResultCode(int code)
{
	switch(code)
	{
		case 3:	 return "IDABORT";
		case 2:	 return "IDCANCEL";
		case 11: return "IDCONTINUE";
		case 5:	 return "IDIGNORE";
		case 7:	 return "IDNO";
		case 1:	 return "IDOK";
		case 4:	 return "IDRETRY";
		case 10: return "IDTRYAGAIN";
		case 6:	 return "IDYES";
	}

	return 0;
}